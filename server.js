const express = require("express");
const app = express();
var bodyParser = require("body-parser");
const port = process.env.PORT || 3000;

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

require("./app/routes")(app, {});
app.listen(port, () => {
  console.log("swapi test is listening on port :: " + port);
});
