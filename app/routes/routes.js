const PeopleAndPlanetService = require("../transformations");
const people = new PeopleAndPlanetService();

async function getData(app) {
  app.get("/people/:sortby?", async (req, res) => {
    people.sortParam = req.params.sortby;
    const allPeople = await people.displayPeople();
    res.send(allPeople);
    res.end();
  });

  app.get("/planets", async (req, res) => {
    const planets = await people.displayPlanets();
    res.send(planets);
    res.end();
  });
}

module.exports = {
  getData,
};
