const { getAllPeople, getAllPlanets, getResidents } = require("./getData");
const sortBy = require("lodash.sortby");

class PeopleAndPlanetService {
  constructor(sortParam) {
    this._sortParam = sortParam;
  }

  get sortParam() {
    return this._sortParam;
  }

  set sortParam(value) {
    this._sortParam = value;
  }

  async displayPeople() {
    return getAllPeople()
      .then((res) => {
        return this.sortParam ? sortBy(res, [this.sortParam]) : res;
      })
      .catch((err) => err);
  }

  async displayPlanets() {
    return getAllPlanets()
      .then((response) => {
        return Promise.all(
          response.map(async (planet) => {
            if (planet.residents.length > 0) {
              const name = await this.getResidentName(planet.residents);
              planet.residents = name;
              return planet;
            }
          })
        );
      })
      .catch((err) => err);
  }

  async getResidentName(residents) {
    return Promise.all(
      residents.map(async (url) => {
        const resp = await getResidents(url);
        return resp.name;
      })
    );
  }
}

module.exports = PeopleAndPlanetService;
