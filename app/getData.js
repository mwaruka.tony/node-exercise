const axios = require("axios");
const { response } = require("express");
const peopleEndpoint = "https://swapi.dev/api/people/";
const planetsEndpoint = "https://swapi.dev/api/planets/";

const getAllPeople = async () => {
  return await getData(peopleEndpoint);
};
const getAllPlanets = async () => {
  return await getData(planetsEndpoint);
};
const getResidents = (residentsEndpoint) => {
  return axios
    .get(residentsEndpoint)
    .then((res) => res.data)
    .catch((err) => err);
};

const getData = async (reqEndpoint) => {
  let chunks = [];
  let page = 1;

  do {
    let { data: response } = await axios.get(reqEndpoint, {
      params: { page: page++ },
    });

    chunks = chunks.concat(response.results);
    if (response.next == null) return chunks;
  } while (response.next !== null);
};

exports.getAllPeople = getAllPeople;
exports.getAllPlanets = getAllPlanets;
exports.getResidents = getResidents;
